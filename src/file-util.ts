import * as fs from "fs";

export const readDataFile = (name: string) : Array<string> => {
 let content = fs.readFileSync(name).toString().replace(/\r/g, '').split("\n");
 return content;
}

export const writeDataFile = (name: string, content: Array<string>) : void => {
 fs.writeFileSync(name, content.join('\n'));
}