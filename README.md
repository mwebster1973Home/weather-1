# Weather Processor

Weather processor is to be used to parse a raw data file containing temperature readings, and produce metrics for each city.

The input file includes a header, followed by data including:

- Country
- City
- Date
- Temp
- Weather

An example data file is included in the data folder.

The results file, contains a header row, then a row for each city with aggregate information for that city:

- City
- Min Temp
- Max Temp
- Average Temp

The results will later be processed down-stream. 
An example result is included in the test, and coding has started, but this may not be correct.

Your exercise is to complete the solution, producing clean code that is easy to maintain. You will need to prove that your result works.

### Further Requirements
- Average temperature for each city should be rounded to one decimal place
- In the result file, the city should be ordered alphabetically
- Any blank input lines should not be processed
- Any spaces should be removed from the input file
- The city name should be InitCapped (eg Birmingham)
- Sometimes the temperature reading goes faulty, and records 999 as the temp, these lines should be discarded
- Duplicate entries for a date/city should be discarded (only process the last one)

Note: A file utility module has been created to deal with file processing.


### Installations
- Run the following to install modules 
  `npm -i`
- Can be compiled by running
  `npm run build`
- Can be executed on example1 file
- `npm run start`
